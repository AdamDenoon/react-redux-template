import React from 'react';
import { render, fireEvent } from 'react-testing-library';
import App from '../../../containers/App';

describe('App', () => {
	it('onboards', () => {
		const { getByTestId } = render(<App />);
		expect(getByTestId('onboarding')).toBeInTheDocument();
	});
});
