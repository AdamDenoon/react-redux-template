import React from 'react';
import { render, fireEvent } from 'react-testing-library';
import App from '../../../containers/App';

describe('App', () => {
	it('navigates to all pages', () => {
		const { getByTestId, getByText, getByLabelText } = render(<App />);
		expect(getByTestId('home-page')).toBeInTheDocument();
		fireEvent.click(getByText('Log in'));
		expect(getByTestId('auth-form')).toBeInTheDocument();
		fireEvent.click(getByText('Sign up'));
		expect(getByLabelText(/username/i)).toBeInTheDocument();
	});
});
